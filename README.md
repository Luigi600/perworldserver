# WorldGrouping | Per world group TabList and chat!

**Note:** This plugin is a fork of the plugin [PerWorldServer](https://github.com/Struck713/PerWorldServer) which was
created by Struck713 and represent an update of PerWorldServer. Due to the new main function of grouping worlds, the
plugin has been renamed, but the structure, commands etc. are still existing. This fork is maintained by Luigi600.

WorldGrouping allows grouping of worlds to separate chats and TabLists, thereby giving the server a BungeeCord feel. A
group determines which worlds share chat messages and TabLists. Players in other groups do not see the players and chat
messages from other groups.

**Remark:** A world without a group is like a group of its own. Only players within this world can communicate and see
each other.

**Remark 2:** Triggering advancements causes chat messages to be sent in each group and each world. Suppression does not
seem to be possible. So disabling within `server.properties` is recommended.

<br />

## Support

Minecraft Spigot server versions tested:

* 1.21 ( v4.3.X )
* 1.20 ( v4.2.X )
* 1.19 ( v4.1.X )
* 1.18 ( v4.0.X )

Not tested, but in theory too:

* 1.17
* 1.16
* 1.15
* 1.14
* 1.13
* 1.12
* 1.11
* 1.10
* 1.9
* 1.8

<br />
<br />

## Installation

The installation of the plugin is extremely simple.

1. just go to the [GitLab releases](https://gitlab.com/Luigi600/worldgrouping/-/releases), select the latest release (
   should be the top one) and click on "**WorldGrouping (JAR)**" under the "other" section
2. move the downloaded JAR file to the `plugins` folder of your server
3. reload/restart the server

<br />
<br />

## Commands

[Repo wiki - commands](https://gitlab.com/Luigi600/worldgrouping/-/wikis/commands)

<br />
<br />

## Screenshots

<details>
  <summary>In ACTION!</summary>

Chat: \
![](https://gitlab.com/Luigi600/worldgrouping/-/wikis/imgs/example-chat.webp)
</details>


<br />
<br />

## Build

**Note:** This information is only interesting for a developer.

Java v15 is used because of the APIs (Bukkit, Spigot) and Maven is used as project management tool. To build the
application through automation (CI/CD), the Docker image `maven:3.8.4-openjdk-17` is used.

To build the JAR file:

```shell
mvn clean package
```

<br />
<br />

## Donation

If you like my work, you can donate for me. By doing so, you show that my work is valued. A donation is not mandatory,
but I would be very happy :)

| Service / Currency | Address                                    | Link / QR Code                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
|--------------------|--------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Bitcoin            | 39S7NWad3FyGVN43etz4K45s9MBaYj8sbp         | ![Bitcoin Donation Address](.gitlab/doc/imgs/donate_bitcoin.png)                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| Ethereum           | 0xBf6722333D72Bd1C0B3C6B3ad970310D1Ea6E83B | ![Ethereum Donation Address](.gitlab/doc/imgs/donate_ethereum.png)                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| Litecoin           | MCi3b77HVTvJFAcDZ3wASsmJRJrmxGgzKj         | ![Litecoin Donation Address](.gitlab/doc/imgs/donate_litecoin.png)                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| PayPal             | payment [at] lui-studio.net                | ![PayPal Donation Address](.gitlab/doc/imgs/donate_paypal.png) <br /> <form action="https://www.paypal.com/donate" method="post" target="_top"><input type="hidden" name="hosted_button_id" value="MBKLD79X3SCT2" /><input type="image" src="https://www.paypalobjects.com/en_US/DE/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" /><img alt="" border="0" src="https://www.paypal.com/en_DE/i/scr/pixel.gif" width="1" height="1" /></form> |
