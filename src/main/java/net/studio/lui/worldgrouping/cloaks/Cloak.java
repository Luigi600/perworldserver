package net.studio.lui.worldgrouping.cloaks;

import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class Cloak {
    private final JavaPlugin plugin;

    protected JavaPlugin getPlugin() {
        return plugin;
    }

    protected Cloak(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    public void show(World world, Player toShow) {
        world.getPlayers()
                .stream()
                .filter(player -> !player.equals(toShow))
                .forEach(player -> show(player, toShow));
    }

    public void hide(World world, Player toHide) {
        world.getPlayers()
                .stream()
                .filter(player -> !player.equals(toHide))
                .forEach(player -> hide(player, toHide));
    }

    public void showAWorld(Player player, World toShow) {
        toShow.getPlayers()
                .stream()
                .filter(streamedPlayer -> !streamedPlayer.equals(player))
                .forEach(streamedPlayer -> show(player, streamedPlayer));
    }

    public void hideAWorld(Player player, World toHide) {
        toHide.getPlayers()
                .stream()
                .filter(streamedPlayer -> !streamedPlayer.equals(player))
                .forEach(streamedPlayer -> hide(player, streamedPlayer));
    }

    public void hideWorldAndPlayer(Player player, World toHide) {
        this.hideAWorld(player, toHide);
        this.hide(toHide, player); // hide player in the old world
    }

    public void showWorldAndPlayer(Player player, World toShow) {
        this.showAWorld(player, toShow);
        this.show(toShow, player);
    }

    public abstract void show(Player player, Player toShow);

    public abstract void hide(Player player, Player toHide);
}
