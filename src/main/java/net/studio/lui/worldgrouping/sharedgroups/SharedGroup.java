package net.studio.lui.worldgrouping.sharedgroups;

import net.studio.lui.worldgrouping.config.ConfigManager;
import net.studio.lui.worldgrouping.config.ConfigNodes;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class SharedGroup {
    private final String groupName;
    private final HashSet<World> worlds = new HashSet<>();

    public String getGroupName() {
        return this.groupName;
    }

    public List<World> getWorlds() {
        return new ArrayList<>(this.worlds);
    }

    public SharedGroup(String grpName, ConfigManager userConfigManager) {
        this.groupName = grpName;

        for (String worldName : userConfigManager.getStringList(ConfigNodes.SHARED_GROUP_LIST + "." + grpName)) {
            World world = Bukkit.getWorld(worldName);
            if (world != null)
                this.worlds.add(world);
        }
    }

    public void addWorld(World world) {
        this.worlds.add(world);
    }

    public boolean removeWorld(World world) {
        return this.worlds.remove(world);
    }

    public List<Player> getAllPlayers() {
        ArrayList<Player> result = new ArrayList<>();

        this.worlds.forEach(w -> result.addAll(w.getPlayers()));

        return result;
    }
}
