package net.studio.lui.worldgrouping.config;

import org.bukkit.entity.Player;

public final class ConfigPermissions {
    private static final String COMMAND_PREFIX = "wg";

    public static final String WILDCARD = COMMAND_PREFIX + ".*";
    public static final String ADMIN = COMMAND_PREFIX + ".admin";
    public static final String GLOBAL_CHAT_TOGGLE = COMMAND_PREFIX + ".chat.global.toggle";
    public static final String INFO = COMMAND_PREFIX + ".info";
    public static final String INFO_ADVANCED = COMMAND_PREFIX + ".info.advanced";
    public static final String GROUPING_MANAGER = COMMAND_PREFIX + ".grouping-manager";
    public static final String ACROSS_CHAT = COMMAND_PREFIX + ".chat.across.message";

    private ConfigPermissions() {
    }

    public static boolean hasPermission(Player player, String... permissions) {
        for (String permission : permissions) {
            if (!player.hasPermission(permission)) {
                return false;
            }
        }
        return true;
    }
}
