package net.studio.lui.worldgrouping;

import com.github.zafarkhaja.semver.Version;
import net.studio.lui.worldgrouping.commands.WorldGroupingCommand;
import net.studio.lui.worldgrouping.config.ConfigManager;
import net.studio.lui.worldgrouping.config.ConfigNodes;
import net.studio.lui.worldgrouping.listeners.PlayerListener;
import net.studio.lui.worldgrouping.listeners.WorldChatListener;
import net.studio.lui.worldgrouping.listeners.WorldSwitchListener;
import net.studio.lui.worldgrouping.manager.ChatManager;
import net.studio.lui.worldgrouping.manager.GlobalManager;
import net.studio.lui.worldgrouping.manager.SharedGroupManager;
import net.studio.lui.worldgrouping.cloaks.Cloak;
import net.studio.lui.worldgrouping.update.GitLabUpdateChecker;
import net.studio.lui.worldgrouping.cloaks.Cloak_1_18_R1;
import net.studio.lui.worldgrouping.cloaks.Cloak_1_9_R1;
import org.bukkit.Bukkit;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Optional;

public class WorldGrouping extends JavaPlugin {
    private final static String URL_PLUGIN_DOWNLOAD = "https://gitlab.com/Luigi600/worldgrouping/-/releases";

    private static final String CURRENT_CONFIG_VERSION = "4.1.1";
    private static final String CURRENT_USER_CONFIG_VERSION = "1.0.0";

    private Cloak cloak;

    @Override
    public void onEnable() {
        final PluginManager pluginManager = Bukkit.getPluginManager();

        final ConfigManager configManager = new ConfigManager(this, CURRENT_CONFIG_VERSION, "config.yml");
        configManager.defaults();

        final ConfigManager userConfigManager = new ConfigManager(this, CURRENT_USER_CONFIG_VERSION, "user-config.yml");
        userConfigManager.defaults();

        final GlobalManager globalManager = new GlobalManager(configManager, userConfigManager);
        final SharedGroupManager sharedGroupManager = new SharedGroupManager(userConfigManager);
        final ChatManager chatManager = new ChatManager();

        final Optional<Version> version = Version.tryParse(getServer().getBukkitVersion());
        final String apiVersion = version.isEmpty()
                ? null
                : new Version.Builder()
                .setMajorVersion(version.get().majorVersion())
                .setMinorVersion(version.get().minorVersion())
                .build().toString();

        switch (apiVersion) {
            case "1.9.0":
            case "1.8.0":
                this.cloak = new Cloak_1_9_R1(this);
                break;
            case "1.21.0":
            case "1.20.0":
            case "1.19.0":
            case "1.18.0":
            case "1.17.0":
            case "1.16.0":
            case "1.15.0":
            case "1.14.0":
            case "1.13.0":
            case "1.12.0":
            case "1.11.0":
            case "1.10.0":
                this.cloak = new Cloak_1_18_R1(this);
                break;
            default:
                getServer().getLogger().warning("You have a version that does not officially support Minecraft version %s. Deactivate the plugin if problems occur or check for updates.");
                this.cloak = new Cloak_1_18_R1(this);
                break;
        }

        WorldSwitchListener worldSwitchListener = new WorldSwitchListener(configManager, sharedGroupManager, this.cloak);
        PlayerListener playerListener = new PlayerListener(configManager, userConfigManager, globalManager, sharedGroupManager, this.cloak);
        worldSwitchListener.addPlayerChangeGroupListener(playerListener);

        pluginManager.registerEvents(worldSwitchListener, this);
        pluginManager.registerEvents(playerListener, this);
        pluginManager.registerEvents(new WorldChatListener(configManager, globalManager, sharedGroupManager, chatManager), this);

        final PluginCommand wgCommand = this.getCommand("worldgrouping");
        assert wgCommand != null;
        wgCommand.setExecutor(new WorldGroupingCommand(configManager, userConfigManager, globalManager, sharedGroupManager, chatManager, this.cloak));

        this.checkOnUpdates(configManager);
    }

    private void checkOnUpdates(ConfigManager configManager) {
        GitLabUpdateChecker updateChecker = new GitLabUpdateChecker(
                this,
                configManager.getBoolean(ConfigNodes.UPDATES_ENABLED)
        );

        switch (updateChecker.check()) {
            case OUT_OF_DATE:
                getLogger().warning(
                        String.format(
                                "A new version (%s) is available on %s.",
                                updateChecker.getNewVersion().toString(),
                                URL_PLUGIN_DOWNLOAD
                        )
                );
                break;
            case UP_TO_DATE:
                getLogger().info(
                        "Uses the latest version."
                );
                break;
            case EMPTY_LIST:
                getLogger().warning(
                        String.format(
                                "The list of versions of the plugin are empty. Update check failed! Please check manually for updates (%s).",
                                URL_PLUGIN_DOWNLOAD
                        )
                );
                break;
            case INVALID_PLUGIN_VERSION_SCHEMA:
                getLogger().warning(
                        String.format(
                                "The version of the plugin does not match the scheme of a version. Update check failed! Please check manually for updates (%s).",
                                URL_PLUGIN_DOWNLOAD
                        )
                );
                break;
            default:
                break;
        }
    }
}
