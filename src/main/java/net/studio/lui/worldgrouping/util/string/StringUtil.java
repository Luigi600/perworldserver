package net.studio.lui.worldgrouping.util.string;

import org.bukkit.ChatColor;

import java.util.List;

public final class StringUtil {
    private StringUtil() {
    }

    public static String color(final String toColor) {
        return color(toColor, '&');
    }

    public static String color(final String toColor, final char alt) {
        return ChatColor.translateAlternateColorCodes(alt, toColor);
    }

    public static String placeholder(String toFill, final StringPlaceholder... placeholders) {
        for (final StringPlaceholder placeholder : placeholders)
            toFill = toFill.replaceAll(placeholder.getPlaceholder(), placeholder.getFill());

        return toFill;
    }

    public static String seperatedList(List<String> list) {
        if (list.size() <= 0)
            return "None";

        return String.join(", ", list);
    }
}
