package net.studio.lui.worldgrouping.listeners;

import net.md_5.bungee.api.chat.TranslatableComponent;
import net.studio.lui.worldgrouping.cloaks.Cloak;
import net.studio.lui.worldgrouping.config.ConfigManager;
import net.studio.lui.worldgrouping.config.ConfigNodes;
import net.studio.lui.worldgrouping.config.ConfigPermissions;
import net.studio.lui.worldgrouping.manager.GlobalManager;
import net.studio.lui.worldgrouping.manager.SharedGroupManager;
import net.studio.lui.worldgrouping.sharedgroups.SharedGroup;
import net.studio.lui.worldgrouping.util.ChatUtil;
import net.studio.lui.worldgrouping.util.string.StringUtil;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class PlayerListener implements Listener, PlayerChangeGroupListener {
    private final boolean enabled;
    private final ConfigManager configManager;
    private final ConfigManager userConfigManager;
    private final GlobalManager globalManager;
    private final SharedGroupManager sharedGroupManager;
    private final Cloak cloak;

    public PlayerListener(ConfigManager configManager, ConfigManager userConfigManager, GlobalManager globalManager, SharedGroupManager sharedGroupManager, Cloak cloak) {
        this.configManager = configManager;
        this.userConfigManager = userConfigManager;
        this.globalManager = globalManager;
        this.sharedGroupManager = sharedGroupManager;
        this.cloak = cloak;

        // again with the cool shorthand
        if (!(this.enabled = configManager.getBoolean(ConfigNodes.CHAT_ENABLED))
        ) {
            ConsoleCommandSender sender = Bukkit.getConsoleSender();
            sender.sendMessage(StringUtil.color("&r[WorldGrouping] &lNOTE:&r You have set per world tab to: &cdisabled&r."));
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        if (!this.enabled)
            return;

        Player player = event.getPlayer();
        World currentWorld = player.getWorld();
        Optional<SharedGroup> sharedGroup = sharedGroupManager.getSharedGroupFromWorld(currentWorld);
        Stream<World> invisibleWorlds;
        Stream<World> visibleWorlds;

        if (sharedGroup.isEmpty()) {
            // take all the worlds but the current one
            invisibleWorlds = Bukkit.getWorlds()
                    .stream()
                    .filter(world -> !world.equals(currentWorld));

            visibleWorlds = Stream.of(currentWorld);
        } else {
            SharedGroup sharedGrp = sharedGroup.get();
            invisibleWorlds = Bukkit.getWorlds()
                    .stream()
                    .filter(w -> !sharedGrp.getWorlds().contains(w));
            visibleWorlds = sharedGrp.getWorlds().stream();
        }

        invisibleWorlds.forEach(world -> this.cloak.hideWorldAndPlayer(player, world));

        this.sendJoinMessages(event, visibleWorlds);
        this.restoreGlobalChatState(player);
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        if (!this.enabled)
            return;

        Player player = event.getPlayer();
        World currentWorld = player.getWorld();

        Optional<SharedGroup> sharedGroup = sharedGroupManager.getSharedGroupFromWorld(currentWorld);
        Stream<World> visibleWorlds;
        if (sharedGroup.isEmpty()) {
            visibleWorlds = Stream.of(currentWorld);
        } else {
            SharedGroup sharedGrp = sharedGroup.get();
            visibleWorlds = sharedGrp.getWorlds().stream();
        }

        this.sendLeftMessages(event, visibleWorlds);
    }

    @Override
    public void onLeftGroup(Player player, List<World> worlds) {
        if (!this.enabled)
            return;

        TranslatableComponent leftMessage = ChatUtil.getLeftMessage(player);
        worlds.forEach(w -> {
            for (Player plyr : w.getPlayers())
                plyr.spigot().sendMessage(leftMessage);
        });
    }

    @Override
    public void onJoinedGroup(Player player, List<World> worlds) {
        if (!this.enabled)
            return;

        TranslatableComponent joinedMessage = ChatUtil.getJoinedMessage(player);
        worlds.forEach(w -> {
            for (Player plyr : w.getPlayers())
                plyr.spigot().sendMessage(joinedMessage);
        });
    }

    private void sendJoinMessages(PlayerJoinEvent event, Stream<World> visibleWorlds) {
        Player player = event.getPlayer();
        event.setJoinMessage(""); // clear joinMessage
        // send message to anyone who is in the same chat and TabList
        TranslatableComponent joinedMessage = ChatUtil.getJoinedMessage(player);
        visibleWorlds.forEach(w -> {
            for (Player plyr : w.getPlayers())
                plyr.spigot().sendMessage(joinedMessage);
        });
    }

    private void sendLeftMessages(PlayerQuitEvent event, Stream<World> visibleWorlds) {
        Player player = event.getPlayer();
        event.setQuitMessage("");
        TranslatableComponent leftMessage = ChatUtil.getLeftMessage(player);
        visibleWorlds.forEach(w -> {
            for (Player plyr : w.getPlayers())
                plyr.spigot().sendMessage(leftMessage);
        });
    }

    private void restoreGlobalChatState(Player player) {
        if (!globalManager.hasGlobalChat(player) &&
                userConfigManager.getStringList(ConfigNodes.GLOBAL_CHAT_USERS)
                        .stream()
                        .anyMatch(p -> p.equals(player.getUniqueId().toString())) &&
                ConfigPermissions.hasPermission(player, ConfigPermissions.GLOBAL_CHAT_TOGGLE)
        ) {
            globalManager.toggle(player);
        }
    }
}
