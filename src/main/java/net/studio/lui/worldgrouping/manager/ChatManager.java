package net.studio.lui.worldgrouping.manager;

import org.bukkit.World;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ChatManager {
    private final List<ChatMessage> messages = new ArrayList<>();

    public ChatManager() {
    }

    @Nullable
    public List<World> getTargets(Player player, String message) {
        final Optional<ChatMessage> msg = this.messages.stream().filter(m -> m.getPlayer().equals(player) && m.getMessage().equals(message)).findFirst();

        return msg.map(ChatMessage::getTarget).orElse(null);
    }

    public void addMessage(ChatMessage chatMessage) {
        this.messages.add(chatMessage);
    }

    public boolean removeMessage(Player player, String message) {
        final Optional<ChatMessage> msg = this.messages.stream().filter(m -> m.getPlayer().equals(player) && m.getMessage().equals(message)).findFirst();

        if (msg.isPresent()) {
            this.messages.remove(msg.get());
            return true;
        }

        return false;
    }
}
