package net.studio.lui.worldgrouping.manager;

import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class ChatMessage {
    private final Player player;
    private final String message;
    private final List<World> target = new ArrayList<>();

    public Player getPlayer() {
        return player;
    }

    public String getMessage() {
        return message;
    }

    public List<World> getTarget() {
        return target;
    }

    public ChatMessage(Player player, String message, List<World> target) {
        this.player = player;
        this.message = message;
        this.target.addAll(target);
    }
}
