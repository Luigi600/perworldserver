package net.studio.lui.worldgrouping.commands;

import net.studio.lui.worldgrouping.config.ConfigManager;
import net.studio.lui.worldgrouping.config.ConfigNodes;
import net.studio.lui.worldgrouping.config.ConfigPermissions;
import net.studio.lui.worldgrouping.manager.ChatManager;
import net.studio.lui.worldgrouping.manager.ChatMessage;
import net.studio.lui.worldgrouping.manager.SharedGroupManager;
import net.studio.lui.worldgrouping.sharedgroups.SharedGroup;
import net.studio.lui.worldgrouping.util.string.StringPlaceholder;
import net.studio.lui.worldgrouping.util.string.StringUtil;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Handles the command "msg".
 */
public class MessageCommandHandler extends CommandPlayerHandler {
    private final static int AMOUNT_OF_COMMAND_ARGS = 3;

    private final SharedGroupManager sharedGroupManager;
    private final ChatManager chatManager;

    public MessageCommandHandler(ConfigManager configManager, SharedGroupManager sharedGroupManager, ChatManager chatManager) {
        super(configManager, "msg", AMOUNT_OF_COMMAND_ARGS, ConfigPermissions.ACROSS_CHAT);

        this.sharedGroupManager = sharedGroupManager;
        this.chatManager = chatManager;
    }

    @Override
    protected boolean handlePlayerCommand(Player player, String[] args) {
        // check if group exists
        final String groupName = args[1];
        Optional<SharedGroup> sharedGroup = sharedGroupManager.getSharedGroupFromName(groupName);
        if (sharedGroup.isEmpty()) {
            player.sendMessage(
                    StringUtil.placeholder(
                            configManager.getLanguageValue(ConfigNodes.GROUP_MANAGER_NOT_EXIST),
                            new StringPlaceholder("%group%", groupName)
                    )
            );
            return true;
        }

        final String message = Arrays.stream(args).skip(2).collect(Collectors.joining(" "));
        this.chatManager.addMessage(new ChatMessage(player, message, sharedGroup.get().getWorlds()));
        player.chat(message);

        return true;
    }
}
