package net.studio.lui.worldgrouping.commands;

import net.studio.lui.worldgrouping.config.ConfigManager;
import net.studio.lui.worldgrouping.config.ConfigNodes;
import net.studio.lui.worldgrouping.config.ConfigPermissions;
import net.studio.lui.worldgrouping.manager.SharedGroupManager;
import net.studio.lui.worldgrouping.util.string.StringPlaceholder;
import net.studio.lui.worldgrouping.util.string.StringUtil;
import org.bukkit.World;
import org.bukkit.command.CommandSender;

import java.util.stream.Collectors;

/**
 * Handles the command "groups".
 */
public class GroupsCommandHandler extends CommandAdminHandler {
    protected final SharedGroupManager sharedGroupManager;

    public GroupsCommandHandler(ConfigManager configManager, SharedGroupManager sharedGroupManager) {
        super(configManager, "groups", ConfigPermissions.INFO);

        this.sharedGroupManager = sharedGroupManager;
    }

    @Override
    protected boolean handleConsoleOrAdminCommand(CommandSender commandSender, String[] args) {
        commandSender.sendMessage(configManager.getLanguageValue(ConfigNodes.SHARED_GROUP_LIST_HEADER));

        String listItem = configManager.getLanguageValue(ConfigNodes.SHARED_GROUP_LIST_ITEM);
        this.sharedGroupManager.getSharedGroups().forEach(g -> {
            String worlds = StringUtil.seperatedList(
                    g.getWorlds()
                            .stream()
                            .map(World::getName)
                            .collect(Collectors.toList()));
            commandSender.sendMessage(StringUtil.placeholder(listItem,
                    new StringPlaceholder("%group%", g.getGroupName()),
                    new StringPlaceholder("%worlds%", worlds)));
        });

        return true;
    }
}
