package net.studio.lui.worldgrouping.commands;

import net.studio.lui.worldgrouping.config.ConfigPermissions;

final class Constants {
    private Constants() {
    }

    public final static String[] COMMAND_NAME_LISTENERS = new String[]{"gp", "worldgrouping"};
    public final static String[] DEFAULT_PERMISSION = new String[]{ConfigPermissions.GLOBAL_CHAT_TOGGLE};
    public final static int DEFAULT_AMOUNT_OF_COMMAND_ARGS = 0;
}
