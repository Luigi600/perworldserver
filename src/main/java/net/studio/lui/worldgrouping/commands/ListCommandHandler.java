package net.studio.lui.worldgrouping.commands;

import net.studio.lui.worldgrouping.config.ConfigManager;
import net.studio.lui.worldgrouping.config.ConfigNodes;
import net.studio.lui.worldgrouping.config.ConfigPermissions;
import net.studio.lui.worldgrouping.util.string.StringPlaceholder;
import net.studio.lui.worldgrouping.util.string.StringUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.stream.Collectors;

/**
 * Handles the command "list".
 */
public class ListCommandHandler extends CommandAdminHandler {
    protected ListCommandHandler(ConfigManager configManager) {
        super(configManager, "list", ConfigPermissions.INFO_ADVANCED);
    }

    @Override
    protected boolean handleConsoleOrAdminCommand(CommandSender commandSender, String[] args) {
        commandSender.sendMessage(configManager.getLanguageValue(ConfigNodes.LIST_HEADER));

        String listItem = configManager.getLanguageValue(ConfigNodes.LIST_ITEM);
        Bukkit.getWorlds().forEach(world -> {
            String players = StringUtil.seperatedList(
                    world.getPlayers()
                            .stream()
                            .map(Player::getName)
                            .collect(Collectors.toList()));
            commandSender.sendMessage(StringUtil.placeholder(listItem,
                    new StringPlaceholder("%world%", world.getName()),
                    new StringPlaceholder("%players%", players)));
        });

        return true;
    }
}
