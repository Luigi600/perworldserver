package net.studio.lui.worldgrouping.commands;

import net.studio.lui.worldgrouping.config.ConfigManager;
import net.studio.lui.worldgrouping.config.ConfigPermissions;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public abstract class CommandHandler<T extends CommandSender> {
    protected final ConfigManager configManager;
    protected final String command;
    protected final String[] commandPermissions;

    public String getCommand() {
        return this.command;
    }

    protected CommandHandler(ConfigManager configManager, String command, String... permissions) {
        this.configManager = configManager;
        this.command = command;
        this.commandPermissions = permissions;
    }

    protected CommandHandler(ConfigManager configManager, String command) {
        this(configManager, command, ConfigPermissions.WILDCARD);
    }

    public abstract boolean handleCommand(CommandSender commandSender, String[] args);

    protected boolean hasPermissionForCommand(Player player) {
        return ConfigPermissions.hasPermission(player, commandPermissions);
    }
}
